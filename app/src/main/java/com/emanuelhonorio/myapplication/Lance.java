package com.emanuelhonorio.myapplication;

public class Lance {
    private String usuario;
    private Double valor;

    public Lance(String usuario, Double valor) {
        this.usuario = usuario;
        this.valor = valor;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
