package com.emanuelhonorio.myapplication;

public class Leilao {

    private String descricao;
    private Lance maiorLance;

    public Leilao(String descricao) {
        this.descricao = descricao;
    }

    public void propoe(Lance lance) {
        if (lance == null) throw new IllegalArgumentException();

        fazAlgo();

        if (maiorLance == null) {
            this.maiorLance = lance;
        }  else if (lance.getValor() > maiorLance.getValor()) {
            maiorLance = lance;
        }
    }

    public void fazAlgo() {

    }

    public String getDescricao() {
        return descricao;
    }

    public Lance getMaiorLance() {
        return maiorLance;
    }
}
