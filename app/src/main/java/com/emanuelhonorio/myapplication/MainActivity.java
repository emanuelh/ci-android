package com.emanuelhonorio.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;


public class MainActivity extends AppCompatActivity {

    private TextView mText;
    private Button btnAskAgain;
    private String m_Text = "";
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mText = findViewById(R.id.m_text);
        btnAskAgain = findViewById(R.id.btn_again);

        sharedPref = getPreferences(Context.MODE_PRIVATE);

        String m_Text = sharedPref.getString("SP", null);
        mText.setText(m_Text);

        if (m_Text == null) {
            askSP();
        }

        btnAskAgain.setOnClickListener(v -> {
            askSP();
        });
    }

    public void askSP() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Qual seu SP? ");
        builder.setMessage("Olhe o número atrás do seu celular");
        builder.setCancelable(false);

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setKeyListener(DigitsKeyListener.getInstance("SP0123456789-"));
        builder.setView(input);

        SimpleMaskFormatter smf = new SimpleMaskFormatter("SPNN-NN");
        MaskTextWatcher mtw = new MaskTextWatcher(input, smf);
        input.addTextChangedListener(mtw);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("SP", m_Text);
                editor.commit();

                String m_Text = sharedPref.getString("SP", null);
                mText.setText(m_Text);
            }
        });

        builder.show();
    }
}