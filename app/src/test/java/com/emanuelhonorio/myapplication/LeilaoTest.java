package com.emanuelhonorio.myapplication;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class LeilaoTest {

    @Spy
    private Leilao leilao = new Leilao("Carro");

    @Spy
    private Lance lance = new Lance("Felipe",  200.0);

    @Test
    public void teste() {
        Mockito.doReturn(lance).when(leilao).getMaiorLance();

        leilao.propoe(lance);

        Mockito.verify(leilao, Mockito.times(1)).fazAlgo();


        Lance maiorLance = leilao.getMaiorLance();

        assertEquals(200.0, maiorLance.getValor(), 0.0001);
    }

    @Test
    public void deve_DevolveDescricao_QuandoRecebeDescricao() {
        Leilao leilaoCarro = new Leilao("Carro");
        String descricao = leilaoCarro.getDescricao();

        assertEquals("Carro", descricao);
    }

    @Test
    public void deve_DevolveMaiorLance_QuandoRecebeMaisDeUmLanceEmOrdemCrescente() {
        Leilao leilaoCarro = new Leilao("Carro");
        leilaoCarro.propoe(new Lance("Emanuel", 100.0));
        leilaoCarro.propoe(new Lance("Emanuel", 200.0));
        leilaoCarro.propoe(new Lance("Emanuel", 201.0));

        assertEquals(201.0, leilaoCarro.getMaiorLance().getValor(), 0.0001);
    }

    @Test
    public void deve_DevolveMaiorLance_QuandoRecebeMaisDeUmLanceEmOrdemDecrescente() {
        Leilao leilaoCarro = new Leilao("Carro");
        leilaoCarro.propoe(new Lance("Emanuel", 200.0));
        leilaoCarro.propoe(new Lance("Emanuel", 199.9));
        leilaoCarro.propoe(new Lance("Emanuel", 99.9));

        assertEquals(200.0, leilaoCarro.getMaiorLance().getValor(), 0.0001);
    }

    @Test
    public void deve_LancaExcecao_QuandoRecebeLanceNulo() {
        Leilao leilaoCarro = new Leilao("Carro");
        try {
            leilaoCarro.propoe(null);
            fail();
        } catch (Exception e) {
        }
    }



}